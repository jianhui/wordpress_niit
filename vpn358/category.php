<?php
/**
* The template for displaying Category pages
*
* @link http://codex.wordpress.org/Template_Hierarchy
*
* @package WordPress
* @subpackage Twenty_Fourteen
* @since Twenty Fourteen 1.0
*/
get_header(); ?>
<section id="primary" class="content-area">
    <div id="content" class="container" role="main">
        <div class="row-fluid">
        <div id="main" class="span8 clearfix" >
            <?php if ( have_posts() ) : ?>
            <header class="archive-header">
                <?php
                $link_url = get_the_permalink();
                $term_description = term_description();
                if ( ! empty( $term_description ) ) :
                printf( '<div class="taxonomy-description">%s</div>', $term_description );
                endif;
                ?>
            </header>
            <!-- .archive-header -->
            <ul class="unstyled">
                <?php
                // Start the Loop.
                while ( have_posts() ) : the_post(); 
                    $img = catch_first_image(get_the_content());
                    $have_img = !empty($img);
                ?>
                <li>
                    <div class="row-fluid">
                        <div class="span12 ">
                            <div class="list">
                                <?php if($have_img) {?>
                                <div class="list_img span3" >
                                    <img class="img-polaroid" src="<?php if($have_img) echo($img); ?>" alt=""/>
                                </div>
                                <?php  }?>
                                <div class="span<?php if($have_img) echo '9'; else echo '12';?>">
                                    <div class="list_title">
                                        <h2> <a href="<?php echo $link_url;?>"><?php the_title() ?></a></h2>
                                    </div>
                                    <div class="list_content">
                                        <p class="lead"><?php echo catch_artical_desc(); ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php get_template_part( 'content', get_post_format() );
                        ?>
                    </div>
                    <!-- end row-fluid -->
                </li>
                <?php
                endwhile;
                ?>
            </ul>
            <?php
            // Previous/next page navigation.
            else :
            // If no content, include the "No posts found" template.
            get_template_part( 'content', 'none' );
            endif;
            ?>
            
            </div><!-- #span8 -->
            <?php get_sidebar("sb_homepage"); // sidebar 1 ?>
            </div><!-- #row-fluid -->
            </div><!-- #content -->
            </section><!-- #primary -->
            <?php get_footer(); ?>