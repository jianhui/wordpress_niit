<?php get_header();

if(file_exists(dirname(__FILE__).'/config_index.php')){
  require_once('config_index.php');
}

?>
<div id="content" class="container">
    <div class="row-fluid">
        <div id="main" class="span8 clearfix" role="main">
            <div class="row-fluid">
                <?php echo search_tab("opac.niit.edu.cn", "www.duxiu.com.j.niit.edu.cn", "lib.niit.edu.cn/xd/Public/yidu_edu.html"); ?>

            </div>
            
            <div class="row-fluid">
            <?php
                echo book_tab2acc($book_carousel_cat, $book_carousel_count, $book_carousel_id, $book_carousel_show_desc);
            ?>
            </div>
            <div class="row-fluid">
                <div class="span6">
                <?php echo cat_desc_list(13,"http://lib.niit.edu.cn/test/wp-content/uploads/2013/04/生命从明天开始.jpg",
                "分类简介","什么叫高贵？就是在无论多么狼狈和绝望的时刻都能保持一个“人”的尊严，这个保持尊严的过程就是我们反抗命运、破解绝望的过程，是我们在无助中仰望苍天和扪心自问的过程，是我们在这个世界的艰辛旅程中惟一可能获得内心平安与",
                50,6);?>
            </div>
            <div class="span6">
                <?php echo cat_desc_list(30,"http://lib.niit.edu.cn/test/wp-content/uploads/2013/04/生命从明天开始.jpg",
                "分类简介","什么叫高贵？就是在无论多么狼狈和绝望的时刻都能保持一个“人”的尊严，这个保持尊严的过程就是我们反抗命运、破解绝望的过程，是我们在无助中仰望苍天和扪心自问的过程，是我们在这个世界的艰辛旅程中惟一可能获得内心平安与",
                50,6);?>
            </div>
            </div>
            <div class="row-fluid">
                <?php
                foreach ($cats as $cat) {
                    echo '<div class="span4">';
                    $category = $cat['cat_id'];
                    $cat_img = $cat['cat_image'];
                    $len_content = $cat['length_content'];
                    $count = $cat['count'];
                    //echo cat_list($cat['cat_id'], $cat['count'], $cat['cat_image'], $cat['length_content'], $len_title, $cat['num']);
					echo cat_list_title($cat['cat_id'], $cat['count'],$len_title,true);
                    echo '</div>';
                }
                ?>
            </div>
            
        </div>
        <!-- end #main -->

        <?php get_sidebar("sb_homepage"); // sidebar 1 ?>

    </div>
    <!-- end row-fluid -->
</div> <!-- end #content -->


<?php get_footer(); ?>
