<?php
function vpn358_search_box($atts) {
	extract(shortcode_atts(array(
		'opac' => "opac.niit.edu.cn",
		'duxiu' => "www.duxiu.com",
		'yidu'=> "lib.niit.edu.cn/xd/Public/yidu_edu.html",
	), $atts));
	return search_tab($opac, $duxiu, $yidu); 
}
add_shortcode('vpn358_search_box', 'vpn358_search_box');

function vpn358_desc_list_cats($atts) {
	extract(shortcode_atts(array(
		'cats_id' => '9,10',
		'count' => 7,
		'len_content' => 250,
		'len_title' => 20, 
		'span' => 'span12',

	), $atts));
	$cats = explode(',', $cats_id);
	$content="";
	$content .= '<div class="'.$span.'">';
    $category = $cats_id;
	$content .= cat_desc_list($category ,$img,$title,$desc,$len_title,$count);
    $content .= '</div>';

	 return '<div class="row-fluid">'.$content.'</div>';
}
add_shortcode('cat_desc_list', 'vpn358_desc_list_cats');

function vpn358_list_top_cats($atts) {
	extract(shortcode_atts(array(
		'cats_id' => '9,10',
		'count' => 7,
		'len_content' => 250,
		'len_title' => 20, 
		'span' => 'span12',

	), $atts));
	$cats = explode(',', $cats_id);
	$content="";
	$content .= '<div class="'.$span.'">';
    $category = $cats_id;
	$content .= cat_list_top($category ,$count,$len_title, $len_content);
    $content .= '</div>';

	 return '<div class="row-fluid">'.$content.'</div>';
}
add_shortcode('cat_list_top', 'vpn358_list_top_cats');

function vpn358_img_list_cats($atts) {
	extract(shortcode_atts(array(
		'cats_id' => '9,10',
		'img_count' => 1,
		'len_content' => 50,
		'image_length_title'=>50,
		'img_width' => '80px',
		'img_height' => '80px',
		'title_count' => 6,
		'len_title' => 20, 
		'span' => 'span4',

	), $atts));
	$cats = explode(',', $cats_id);
	$content="";
		
	foreach ($cats as $cat) {
		$info = "";
        $content .= '<div class="'.$span.'">';
        $category = $cat;
		$content .= cat_img_list($category ,$img_count,$len_content,$image_length_title,$img_width,$img_height,$title_count,$len_title);
        $content .= '</div>';
    }  
	 return '<div class="row-fluid">'.$content.'</div>';
}
add_shortcode('cat_img_list', 'vpn358_img_list_cats');

  /*
 *2篇图片+列出标题
 *$category 分类ID
 *$img_count,图片-标题-简介的内容篇数
 *$len_content,图片-标题-简介的简介的字数
 *$image_length_title,图片-标题-简介的标题字数
 *$img_width,图片-标题-简介的图片宽度
 *$img_height,图片-标题-简介的图片高度
 *$title_count,仅列出标题的篇数
 *$len_title,仅列出文章标题的字数
 */
function vpn358_img2_list_cats($atts) {
	extract(shortcode_atts(array(
		'cats_id' => '9,10',
		'img_count' => 2,
		'len_content' => 50,
		'image_length_title'=>50,
		'img_width' => '80px',
		'img_height' => '80px',
		'title_count' => 6,
		'len_title' => 20, 
		'span' => 'span4',

	), $atts));
	$cats = explode(',', $cats_id);
	$content="";
		
	foreach ($cats as $cat) {
		$info = "";
        $content .= '<div class="'.$span.'">';
        $category = $cat;
		$content .= cat_img2_list($category ,$img_count,$len_content,$image_length_title,$img_width,$img_height,$title_count,$len_title);
        $content .= '</div>';
    }  
	 return '<div class="row-fluid">'.$content.'</div>';
}
add_shortcode('cat_img2_list', 'vpn358_img2_list_cats');

function vpn358_book_tab2acc($atts) {
	extract(shortcode_atts(array(
		'cat_id' => '13',
		'count' => 5,
		'id' => "book_tab2acc_".rand(),
		//'len_title' => 20, 
		'show_desc' => false,
	), $atts));

    $content =   book_tab2acc($cat_id,$count,$id,$show_desc);
	return $content;
}
add_shortcode('book_tab2acc', 'vpn358_book_tab2acc');
?>