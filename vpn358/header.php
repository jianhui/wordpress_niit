<?php
/**
 * The Header for Customizr.
 *
 * Displays all of the <head> section and everything up till <div id="main-wrapper">
 *
 * @package Customizr
 * @since Customizr 1.0
 */
?>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>                          
<!--<![endif]-->

<head>
                    <meta charset="<?php bloginfo( 'charset' ); ?>" />
                    <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
                    <title><?php wp_title( '|' , true, 'right' ); ?></title>
                    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                    <link rel="profile" href="http://gmpg.org/xfn/11" />
                    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

                   <!-- Icons font support for IE6-7  -->
                    <!--[if lt IE 8]>
                      <script src="<?php  get_stylesheet_directory_uri(); ?>inc/css/fonts/lte-ie7.js"></script>
                    <![endif]-->
                    <?php wp_head(); ?>
                    <!--Icons size hack for IE8 and less -->
                    <!--[if lt IE 9]>
                      <link href="<?php  get_stylesheet_directory_uri(); ?>inc/css/fonts/ie8-hacks.css" rel="stylesheet" type="text/css"/>
                    <![endif]-->
        </head>

	<body <?php //body_class(); ?> <?php //echo tc__f('tc_body_attributes' , 'itemscope itemtype="http://schema.org/WebPage"') ?>>
		
		<?php //do_action( '__before_header' ); ?>

	   	<header class="<?php echo tc__f('tc_header_classes', 'tc-header clearfix row-fluid') ?>" role="banner">
			
			<?php 
			//the '__header' hook is used by (ordered by priorities) : TC_header_main::$instance->tc_logo_title_display(), TC_header_main::$instance->tc_tagline_display(), TC_header_main::$instance->tc_navbar_display()
			do_action( '__header' ); 

            </script>

		</header>

		<?php 
		 	//This hook is filtered with the slider : TC_slider::$instance->tc_slider_display()
			//do_action ( '__after_header' )
		?>
