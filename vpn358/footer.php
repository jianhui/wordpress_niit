<footer id="footer">
<div></div>
    
    
    <div class="colophon">
        
        <div class="container">
            <div class="row-fluid">
                <div class="span4 credits">
                    
                    <nav class="clearfix">
                        <?php //wp_bootstrap_footer_links(); // Adjust using Menus in Wordpress Admin ?>
                    </nav>
                    <p class="pull-right"><a class="back-to-top" href="#">Back to top</a></p>
                    <p class="attribution">Copyright &copy;    <?php echo date("Y"); bloginfo('name'); ?> </p>
                    
                    <div class="span4 backtop">
                        <p class="pull-right">
                        
                        </p>
                    </div>
                    
                    </div><!-- .row-fluid -->
                    </div><!-- .container -->
                    </div><!-- .colophon -->
                </footer>
                </div> <!-- end #container -->
                
                <?php wp_footer(); // js scripts are inserted using this function ?>
            </body>
        </html>
