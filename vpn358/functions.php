<?php
if(file_exists(dirname(__FILE__).'/config.php')){
require_once('config.php');
}
require_once('vpn358_shotcode.php');
require_once('vpn358_func/vpn358_widget.php');
//require_once('wp_bootstrap_navwalker.php');
//require_once('wp-bootstrap-navmenu.php');
////以下内容不要编辑
/*
function cat_list($category ,$img_count,$len_content,$image_length_title){
$img_width = "80px";
$img_height = "80px";
$title_count=6;
$len_title = 30;
return cat_list_img($category ,$img_count,$len_content,$image_length_title,$img_width,$img_height,$title_count,$len_title);
}*/
/*
*显示简单的文章标题
*$category 分类ID
*$count,篇数
*$len_title,文章标题的字数
*$show_link_url，true时将标题的url设置为link_url字段，false时将标题的url链接设置为文章
*/
function cat_list_title($category ,$count,$len_title, $show_link_url){
ob_start();
?>
<?php query_posts("showposts=$count&cat=$category")?>
<div  class="widget widget_nav_menu panel panel-niit panel-primary">
  
  <div class="panel-heading widgettitle">
    <a class="panel-header" href="<?php echo get_category_link($category);?>">
      <?php single_cat_title();  ?>
      <div class="right_title">
        <span  class="dashicons dashicons-portfolio"></span>
      </div>
    </a>
  </div>
  <ul class="menu">
    <?php
    while (have_posts()) : the_post();
    ?>
    <li class="menu-item menu-item-type-custom menu-item-object-custom ">
      <?php
      $post_id = get_the_ID();
      $link_url=get_post_meta($post_id, 'link_url', $single = true);
      if ( !$show_link_url || !$link_url ) {
      $link_url = get_the_permalink();
      }
      ?>
      <a href="<?php echo $link_url;?>" title="<?php the_title_attribute();?>">
        <?php
        echo mb_strimwidth(get_the_title(),0,$len_title,"...");
        ?>
      </a></li>
      <?php
      endwhile; ?>
    </ul>
    
  </div>
  <?php
  $content = ob_get_contents();
  ob_end_clean();
  return $content;
  }
  
  /*
  *带头条的文章
  *$category 分类ID
  *$len_desc,头条简介的字数
  *$count,篇数
  *$len_title,文章标题的字数
  *$len_content，头条新闻的简介字数
  */
  function cat_list_top($category ,$count,$len_title, $len_content){
  ob_start();
  ?>
  <?php query_posts("showposts=$count&cat=$category")?>
  <div  class="widget widget_nav_menu panel panel-niit panel-primary">
    
    <?php
      $i=0;
    while (have_posts()) : the_post();
    if ($i==0) {
    ?>
    <div class="headlinetop">
      <a href="<?php echo get_the_permalink();?>" target="_blank">
        <h3><?php the_title_attribute();?></h3>
        <p><?php echo mb_strimwidth(strip_tags(apply_filters('the_content', get_the_content())), 0, $len_content,"...");?>...</p>
      </a>
    </div>
    <?php }else{
    if($i==1) echo '<ul class="menu">';
      ?>
      <li class="menu-item menu-item-type-custom menu-item-object-custom ">
        <?php $category=get_the_category(get_the_ID());
        echo '<a href="'.get_category_link( $category[0]->term_id ).'">['.$category[0]->cat_name.']</a>';?>
        <a href="<?php echo get_the_permalink();?>" title="<?php the_title_attribute();?>">
          <?php
          echo mb_strimwidth(get_the_title(),0,$len_title,"...");
          ?>
        </a></li>
        
        
        <?php
          }
            $i++;
        endwhile;
        echo '</ul>';
      ?>
    </div>
    <?php
    $content = ob_get_contents();
    ob_end_clean();
    return $content;
    }
    /*
    *显示简单的文章标题
    *$category 分类ID
    *$img,分类图片
    *$len_content,图片-标题-简介的简介的字数
    *$title,标题
    *$desc,分类简介
    *$len_title,仅列出文章标题的字数
    *$count,列表条数
    */
    function cat_desc_list($category ,$img,$title,$desc,$len_title,$count){
    ob_start();
    ?>
    <div  class="widget widget_nav_menu panel panel-niit panel-primary">
      
      <div class="panel-heading widgettitle">
        <a class="panel-header" href="<?php echo get_category_link($category);?>">
          <?php echo get_the_category_by_ID($category ); ?>
          <div class="right_title">
            <span  class="dashicons dashicons-portfolio"></span>
          </div>
        </a>
      </div>
      <ul class="columnistrecom">
        <li>
          <span class="author">
          <img src="<?php echo $img; ?>"  alt="<?php echo $img_width.":".$img_height; ?>">
          </span>
          <a class="summary" href="<?php echo get_category_link($category);?>" title="<?php echo $title; ?>" target="_blank">
            <h4><?php echo $title;?></h4>
          </a>
          <p><?php echo $desc; ?></p>
          
        </li>
      </ul>
      <ul class="menu">
        <?php query_posts("showposts=$count&cat=$category");
        while (have_posts()) : the_post();
        ?>
        <li class="menu-item menu-item-type-custom menu-item-object-custom ">
          <a href="<?php echo get_the_permalink();?>" title="<?php the_title_attribute();?>">
            <?php
            echo mb_strimwidth(get_the_title(),0,$len_title,"...");
            ?>
          </a>
        </li>
        <?php
        endwhile; ?>
      </ul>
    </div>
    <?php
    $content = ob_get_contents();
    ob_end_clean();
    return $content;
    }
    /*
    *显示简单的文章标题
    *$category 分类ID
    *$img_count,图片-标题-简介的内容篇数
    *$len_content,图片-标题-简介的简介的字数
    *$image_length_title,图片-标题-简介的标题字数
    *$img_width,图片-标题-简介的图片宽度
    *$img_height,图片-标题-简介的图片高度
    *$title_count,仅列出标题的篇数
    *$len_title,仅列出文章标题的字数
    */
    function cat_img_list($category ,$img_count,$len_content,$image_length_title,$img_width,$img_height,$title_count,$len_title){
    ob_start();
    $count=$img_count+$title_count;
    ?>
    <?php query_posts("showposts=$count&cat=$category")?>
    <div  class="widget widget_nav_menu panel panel-niit panel-primary">
      
      <div class="panel-heading widgettitle">
        <a class="panel-header" href="<?php echo get_category_link($category);?>">
          <?php single_cat_title();  ?>
          <div class="right_title">
            <span  class="dashicons dashicons-portfolio"></span>
          </div>
        </a>
      </div>
      <?php
      $i = 0;
      while (have_posts()) : the_post();
      if( $len_content > 0 and $i < $img_count) {
      $artical_img = catch_first_image(get_the_content( ));
      if (empty($artical_img)) $artical_img = get_stylesheet_directory_uri()."/images/cat5.jpg";
        if($i==0) echo '<ul class="columnistrecom">';
        ?>
        <li>
          <span class="author">
          <img src="<?php echo $artical_img; ?>" width="<?php echo $img_width; ?>" height="<?php echo $img_height; ?>" alt="<?php echo $img_width.":".$img_height; ?>">
          </span>
          <a class="summary" href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>" target="_blank">
            <h4><?php
            echo mb_strimwidth(get_the_title(),0,$len_title-4,"...");
            ?></h4>
            <p><?php
            echo mb_strimwidth(strip_tags(apply_filters('the_content', get_the_content())), 0, $len_content,"...");
            ?></p>
          </a>
        </li>
        
        <?php
      if($i==$img_count-1) echo '</ul>';
      }else{
      if($i==$img_count) echo '<ul class="menu">';?>
        
        <li class="menu-item menu-item-type-custom menu-item-object-custom ">
          <a href="<?php echo get_the_permalink();?>" title="<?php the_title_attribute();?>">
            <?php
            echo mb_strimwidth(get_the_title(),0,$len_title,"...");
            ?>
          </a>
        </li>
        <?php
      if($i==$count) echo '</ul>';
      }
      $i = $i+1;
      endwhile; ?>
    </div>
    <?php
    $content = ob_get_contents();
    ob_end_clean();
    return $content;
    }
    
    /*
    *2篇图片+列出标题
    *$category 分类ID
    *$img_count,图片-标题-简介的内容篇数
    *$len_content,图片-标题-简介的简介的字数
    *$image_length_title,图片-标题-简介的标题字数
    *$img_width,图片-标题-简介的图片宽度
    *$img_height,图片-标题-简介的图片高度
    *$title_count,仅列出标题的篇数
    *$len_title,仅列出文章标题的字数
    */
    function cat_img2_list($category ,$img_count,$len_content,$image_length_title,$img_width,$img_height,$title_count,$len_title){
    ob_start();
    $count=$img_count+$title_count;
    ?>
    <?php query_posts("showposts=$count&cat=$category")?>
    <div  class="widget widget_nav_menu panel panel-niit panel-primary">
      
      <div class="panel-heading widgettitle">
        <a class="panel-header" href="<?php echo get_category_link($category);?>">
          <?php single_cat_title();  ?>
          <div class="right_title">
            <span  class="dashicons dashicons-portfolio"></span>
          </div>
        </a>
      </div>
      <?php
      $i = 0;
      while (have_posts()) : the_post();
      if( $len_content > 0 and $i < $img_count) {
      $artical_img = catch_first_image(get_the_content( ));
      if (empty($artical_img)) $artical_img = get_stylesheet_directory_uri()."/images/cat5.jpg";
        if($i==0) echo '<ul class="articletop">';
        ?>
        <li>
          <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?> target="_blank">
            <img src="<?php echo $artical_img; ?>" width="120" height="90" alt="">
            <span class="caption"><?php the_title_attribute(); ?></span>
          </a>
        </li>
        
        <?php
      if($i==$img_count-1) echo '</ul>';
      }else{
      if($i==$img_count) echo '<ul class="menu">';?>
        
        <li class="menu-item menu-item-type-custom menu-item-object-custom ">
          <a href="<?php echo get_the_permalink();?>" title="<?php the_title_attribute();?>">
            <?php
            echo mb_strimwidth(get_the_title(),0,$len_title,"...");
            ?>
          </a></li>
          <?php
        if($i==$count) echo '</ul>';
        }
        $i = $i+1;
        endwhile; ?>
      </div>
      <?php
      $content = ob_get_contents();
      ob_end_clean();
      return $content;
      }
      
      //得到缩略图
      function catch_first_image($content) {
      $first_img = '';
      $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $content, $matches);
      $first_img = @$matches [1] [0];
      if(empty($first_img)){ //Defines a default image
      return '';
      }
      return $first_img;
      }
      class vpn358_nav_walker extends Walker_Nav_Menu
      {
      function start_lvl( &$output, $depth = 0, $args = array() ) {
      $indent = str_repeat("\t", $depth);
      $output .= "\n$indent<ul class=\"dropdown-menu\">\n";
        }
        }
        function register_vpn358_menus() {
        register_nav_menus(
        array('primary_menu' => __( '主菜单' ) )
        );
        }
        add_action( 'init', 'register_vpn358_menus' );
        function book_tab2acc($category,$count,$id,$show_desc=false){
        ob_start();
        $len_title = 6;
        $len_content = 135;
        query_posts("showposts=$count&cat=$category");
        $i = 0;
        $book_imgs  = array();
        while (have_posts()) : the_post();
        $content = get_the_content();
        $txt =strip_tags($content);
        if (mb_strlen($txt)>$len_content) {
        $txt = mb_substr( $txt, 0, $len_content);
        $txt = $txt."...";
        }
        $image = addslashes(catch_first_image($content) );
        $book_imgs[$i] = array("title"=>get_the_title(),
        "img"=>$image,
        "link"=>get_the_permalink(),
        "txt"=>$txt);
        $i++;
        endwhile;
        //var_export($book_imgs);
        ?>
        <div id="<?php echo 'tab2acc-'.$id;?>">
          <ul class="resp-tabs-list">
            <?php
            $ac = 0;
            foreach ($book_imgs as $key => $book) {
            if($ac == 0 ) {
            $active = "active";
            $ac = 1;
            }else{
            $active="";
            }
            $title = $book['title'];
            if (mb_strlen($title)>$len_title) {
            $title = mb_substr($title, 0, $len_title)."...";
            }
            echo '<li>'.$title.'</li>';
            }
            ?>
          </ul>
          <div class="resp-tabs-container">
            <?php
            $ac = 0;
            foreach ($book_imgs as $key => $book) {
            if($ac == 0 ) {
            $active = "active";
            $ac = 1;
            }else{
            $active="";
            }
            $title = $book['title'];
            if (mb_strlen($title)>$len_title) {
            //$title = mb_substr($title, 0,$len_title);
            }
            $image = '<div class="row-fluid"><div class="span6"><a target="_blank" href="'.$book["link"].'"><img height="250px" src="'.stripcslashes($book["img"]).'"></a></div>';
            
            $content = $book["txt"];
            echo '<div>'.$image.'<div class="span6"><a target="_blank" href="'.$book["link"].'"><h3>'.$title.'</h3></a><p>'.$content.'</p></div></div></div>';
            }
            ?>
          </div>
        </div>
        <script type="text/javascript">
        jQuery('#<?php echo 'tab2acc-'.$id;?>').easyResponsiveTabs({type: 'vertical',fit: true});
        </script>
        <?php
        $flush_content = ob_get_contents();
        ob_end_clean();
        return $flush_content;
        }
        function book_carousel($category,$count,$id,$show_desc=false){
        query_posts("showposts=$count&cat=$category");
        $i = 0;
        $book_imgs  = array();
        while (have_posts()) : the_post();
        $content = get_the_content();
        $txt = mb_substr( strip_tags($content), 0, 300);
        $image = addslashes(catch_first_image($content) );
        $book_imgs[$i] = array("title"=>get_the_title(),
        "img"=>$image,
        "link"=>get_the_permalink(),
        "txt"=>$txt);
        $i++;
        endwhile;
        //var_export($book_imgs);
        ?>
        <div id="carousel-book-<?php echo $id;?>" style="height:370px;" class="carousel slide" data-ride="carousel">
          <ol class="carousel-indicators">
            <?php
            $ac = 0;
            foreach ($book_imgs as $key => $book) {
            if($ac == 0 ) {
            $active = "active";
            $ac = 1;
            }else{
            $active="";
            }
            echo '<li data-target="#carousel-book-'.$id.'" data-slide-to="'.$key.'" class="'.$active.'"></li>';
            }
            ?>
          </ol>
          <div class="carousel-inner">
            <?php
            $ac = 0;
            foreach ($book_imgs as $key => $book) {
            if($ac == 0 ) {
            $active = "active";
            $ac++;
            }else{
            $active="";
            }
            ?>
            <div class="item <?php echo $active;?>">
              
              <div class="row-fluid">
                <div class="span4 clearfix">
                  <a href="<?php echo $book['link'];?>">
                    <img class=""
                    style="margin-top:10px;"
                    data-src="holder.js/350x350/auto/#777:#555/text:<?php echo $book['title'];?> "
                    alt="<?php echo $book['title'];?>"
                    src="<?php echo $book['img'];?>" width="350px" height="350px">
                  </a>
                </div>
                <?php if ($show_desc == true) { ?>
                <div class="span8 clearfix">
                  <div class="">
                    <a href="<?php echo $book['link'];?>">
                      <h3><?php echo $book['title'];?></h3>
                    </a>
                    <p><?php echo $book['txt'];?>...<br />
                    <a href="<?php echo $book['link'];?>">(阅读更多)</a></p>
                    
                  </div>
                </div>
                <?php }; ?>
                
              </div>
            </div>
            <?php
            }
            ?>
          </div>
          <a class="left carousel-control" href="#carousel-book-<?php echo $id;?>" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
          </a>
          <a class="right carousel-control" href="#carousel-book-<?php echo $id;?>" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
          </a>
        </div>
        <?php
        }
        function register_button( $buttons ) {
        array_push( $buttons, "|", "recentposts" );
        return $buttons;
        }
        function add_plugin( $plugin_array ) {
        $plugin_array['recentposts'] = get_stylesheet_directory_uri() . '/vpn358_func/vpn358_short.js';
        return $plugin_array;
        }
        function my_recent_posts_button() {
        
        if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') ) {
        return;
        }
        
        if ( get_user_option('rich_editing') == 'true' ) {
        add_filter( 'mce_external_plugins', 'add_plugin' );
        add_filter( 'mce_buttons', 'register_button' );
        }
        
        }
        add_action('init', 'my_recent_posts_button');
        ///
        function vpn358_register_sidebars() {
        register_sidebar(array(
        'id' => 'sidebar1',
        'name' => 'Main Sidebar',
        'description' => 'Used on every page BUT the homepage page template.',
        'before_widget' => '<div id="%1$s" class="widget %2$s panel-niit panel-primary">',
        'after_widget' => '</div>',
        'before_title' => '<div class="panel-heading widgettitle">',
        'after_title' => '</div>',
        ));
        register_sidebar(array(
        'id' => 'sb_homepage',
        'name' => 'Homepage Sidebar',
        'description' => 'Used only on the homepage page template.',
        'before_widget' => '<div id="%1$s" class="widget %2$s panel-niit panel-primary">',
        'after_widget' => '</div>',
        'before_title' => '<h4 class="widgettitle">',
        'after_title' => '</h4>',
        ));
        register_sidebar(array(
        'id' => 'footer1',
        'name' => 'Footer 1',
        'before_widget' => '<div id="%1$s" class="widget span4 %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h4 class="widgettitle">',
        'after_title' => '</h4>',
        ));
        }
        vpn358_register_sidebars();
        //前台启用图标字体
        add_action( 'wp_enqueue_scripts', 'vpn358_load_dashicons' );
        function vpn358_load_dashicons() {
        wp_enqueue_style( 'dashicons' );
        }
        // enqueue styles
        if( !function_exists("vpn358_theme_styles") ) {
        function vpn358_theme_styles() {
        // This is the compiled css file from LESS - this means you compile the LESS file locally and put it in the appropriate directory if you want to make any changes to the master bootstrap.css.
        wp_register_style( 'Tabs-to-Accordion', get_stylesheet_directory_uri() . '/css/easy-responsive-tabs.css', array(), '1.0', 'all' );
        wp_enqueue_style( 'Tabs-to-Accordion' );
        }
        }
        add_action( 'wp_enqueue_scripts', 'vpn358_theme_styles' );
        // enqueue javascript
        if( !function_exists( "vpn358_theme_js" ) ) {
        function vpn358_theme_js(){
        wp_register_script( 'Tabs-to-Accordion',get_stylesheet_directory_uri() . '/js/easyResponsiveTabs.js',array('jquery'),'1.2' );
        wp_enqueue_script('Tabs-to-Accordion');
        }
        }
        add_action( 'wp_enqueue_scripts', 'vpn358_theme_js' );
        //禁用Open Sans
        class Disable_Google_Fonts {
        public function __construct() {
        add_filter( 'gettext_with_context', array( $this, 'disable_open_sans'             ), 888, 4 );
        }
        public function disable_open_sans( $translations, $text, $context, $domain ) {
        if ( 'Open Sans font: on or off' == $context && 'on' == $text ) {
        $translations = 'off';
        }
        return $translations;
        }
        }
        $disable_google_fonts = new Disable_Google_Fonts;
        
        function vpn358_admin_function(){
        add_theme_page( 'vpn358主题管理', 'vpn358主题设置', 'administrator', 'ashu_slug','vpn358_admin_display_function');
        }
        
        function vpn358_admin_display_function(){
        echo '<h1>vpn358主题设置页面</h1>
        <h4><a href="http: //www.vpn358.com">customizr子主题，
          作者:Zhangyc，
        网址:http: //www.vpn358.com/</a></h4><br />';
        /*
        global $wpdb;
        $request = "SELECT $wpdb->terms.term_id, name FROM $wpdb->terms ";
        $request .= " LEFT JOIN $wpdb->term_taxonomy ON $wpdb->term_taxonomy.term_id = $wpdb->terms.term_id ";
        $request .= " WHERE $wpdb->term_taxonomy.taxonomy = 'category' ";
        $request .= " ORDER BY term_id asc";
        $categorys = $wpdb->get_results($request);
        */
        echo '<table><tr><td>分类名称</td><td>分类ID</td><td>文章篇数</td></tr>';
        $categorys = get_terms('category', 'orderby=name' );
        foreach($categorys as $category) {
        //$cat_name = get_cat_name($category);
        
        $output = '<tr><td>'.$category->name."</td><td><em>".$category->term_id.'</em></td><td>'.$category->count.'</td></tr>';
        echo $output;
        }
      echo '</table>';
      }
      add_action('admin_menu', 'vpn358_admin_function');
      add_filter( 'embed_oembed_discover', '__return_true' );
      wp_oembed_add_provider( '#http://v.youku.com/v_show/id_(.*?).html#i', 'http://player.youku.com/embed', true );
      ?>