<?php

function search_tab($opac_uri,$duxiu_uri,$yidu_uri){
	ob_start();
  ?>
  <!-- Nav tabs -->
  <div class="bs-docs-example">
    
    <ul class="nav nav-tabs" id="search_tab">
      <?php if( !empty($opac_uri) ) { ?>
      <li class="active"><a href="#OPAC" data-toggle="tab" >图书馆OPAC查询</a></li>
      <?php }?>
      <?php if( !empty($duxiu_uri) ) { ?>
      <li><a href="#duxiu" data-toggle="tab">读秀一站式检索</a></li>
      <?php }?>
      <?php if( !empty($yidu_uri) ) { ?>
      <li><a href="#yidu" data-toggle="tab">Calis检索E读</a></li>
      <?php }?>
      
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <?php if( empty($opac_uri) ) $opac_uri="opac.niit.edu.cn"; ?>
      <div class="tab-pane active" id="OPAC">
        <div class="search_bar opac">
          
          <form id="f" name="f" target="blank" method="get" action="http://<?php echo $opac_uri;?>/opac/openlink.php">
            <div class="row-fluid">
              <div class="span12"><div class="input-prepend">
                <select name="strSearchType" >
                  <option value="title">题&nbsp;&nbsp;名</option>
                  <option value="author">责任者</option>
                  <option value="keyword">主题词</option>
                  <option value="isbn">ISBN/ISSN</option>
                  <option value="asordno">订购号</option>
                  <option value="coden">分类号</option>
                  <option value="callno">索书号</option>
                  <option value="publisher">出版社</option>
                  <option value="series">丛书名</option>
                  <option value="tpinyin">题名拼音</option>
                  <option value="apinyin">责任者拼音</option>
                </select>
                
                
                <input type="text" class="paddingleft input-xlarge"  id="strText" name="strText" >
                
                <button type="submit" value="检索" class="btn btn-primary">检索</button>
                <input type="hidden" name="__hash__" value="3bde5e984485f978fd388358dc02c73b">
              </div>
            </div>
            
          </div>
        </form>
      </div>
    </div>
    <?php ?>
    <?php if( empty($duxiu_uri) ) $duxiu_uri="www.duxiu.com";  ?>
    <div class="tab-pane" id="duxiu">
      <div class="search_bar duxiu">
        
        <iframe allowtransparency="true" id="duxiuframe14" border="0" vspace="0" hspace="0" marginwidth="0" marginheight="0" framespacing="0" frameborder="0" scrolling="no" width="750" height="56" src="http://<?php echo $duxiu_uri;?>/pop/isearch.jsp?style=14&amp;sw=输入检索词&amp;logo=&amp;bimg=&amp;swsize=60&amp;enc=utf-8"></iframe>
        
      </div>
    </div>
    <?php ?>
    <?php if( empty($yidu_uri)) $yidu_uri ="lib.niit.edu.cn/xd/Public/yidu_edu.html";  ?>
    <div class="tab-pane" id="yidu">
      <div class="search_bar yidu">
        <iframe name="frame4" width="100%" height="150" frameborder="0" scrolling="no" src="http://<?php echo $yidu_uri;?>" allowtransparency="true"></iframe>
      </div>
    </div>
    <?php ?>
    
  </div>
</div>
<script>
jQuery(function () {
jQuery('#search_tab a:first').tab('show')
})
</script>
<?
$content = ob_get_contents();
ob_end_clean();
return $content;
}

/////// 带过滤功能的最新文章
//Adds vpn358_recent_posts_widget widget.
class vpn358_recent_posts_widget extends WP_Widget {
//Register widget with WordPress.
public function __construct() {
parent::__construct(
'vpn358_recent_posts_widget', // Base ID
'vpn358最新文章', // Name
array( 'description' => '带分类过滤功能的最新文章', ) // Args
);
}
public function widget( $args, $instance ) {
extract( $args );
$title = apply_filters( 'widget_title', $instance['title'] );
$post_num = apply_filters( 'widget_title', $instance['post_num'] );
$catID = apply_filters( 'widget_title', $instance['catID'] );
echo $before_widget;
if ( ! empty( $title ) ){
?>

<?php
echo $before_title.'<a class="panel-header" href="'.get_category_link($catID).'">'.$title;
echo '<div class="right_title"><span  class="dashicons dashicons-portfolio"></span></div></a>';
echo  $after_title;
}
?>
<ol >
  <?php
  global $post;
  $tmp_post = $post;
  $recent_posts = get_posts('orderby=ASC&numberposts='.$post_num.'&category='.$catID);
  foreach( $recent_posts as $post ) { setup_postdata($post); ?>
  <li style="list-style-type:square;color:#428bca;"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
    <?php
    echo mb_strimwidth(get_the_title(),0,38,"...");
  ?></a>
</li>
<?php } $post = $tmp_post; setup_postdata($post); ?>
</ol>
<?php
echo $after_widget;
}
public function update( $new_instance, $old_instance ) {
$instance = array();
$instance['title'] = strip_tags( $new_instance['title'] );
$instance['post_num'] = strip_tags( $new_instance['post_num'] );
$instance['catID'] = strip_tags( $new_instance['catID'] );
return $instance;
}
public function form( $instance ) {
if ( isset( $instance[ 'title' ] ) ) {
$title = $instance[ 'title' ];
}
else {
$title = 'Recent Posts';
}
if ( isset( $instance[ 'post_num' ] ) ) {
$post_num = $instance[ 'post_num' ];
}
else {
$post_num = 5;
}
if ( isset( $instance[ 'catID' ] ) ) {
$catID = $instance[ 'catID' ];
}
else {
$catID = '';
}
?>
<p>
<label for="<?php echo $this->get_field_id( 'title' ); ?>">标题:</label>
<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
<label for="<?php echo $this->get_field_id( 'post_num' ); ?>">显示数量:</label>
<input class="widefat" id="<?php echo $this->get_field_id( 'post_num' ); ?>" name="<?php echo $this->get_field_name( 'post_num' ); ?>" type="text" value="<?php echo esc_attr( $post_num ); ?>" />
<label for="<?php echo $this->get_field_id( 'slug' ); ?>">选择分类:</label>
<?php
wp_dropdown_categories(array(
'name' => $this->get_field_name( 'catID' ),
'hide_empty' => 0,
'orderby' => 'name',
'show_count' => 1,
'selected' => esc_attr( $catID ),
'hierarchical' => true,
'show_option_none' =>'全部分类',
'echo' => 1
));
?>
</p>
<?php
}
} // class zww_recent_posts_widget
// register zww_recent_posts_widget widget
add_action( 'widgets_init', create_function( '', 'register_widget( "vpn358_recent_posts_widget" );' ) );
?>