<?php
if(file_exists(dirname(__FILE__).'/config.php')){
  require_once('config.php');
}

require_once('vpn358_shotcode.php');
require_once('vpn358_func/vpn358_widget.php');
//require_once('wp_bootstrap_navwalker.php');
//require_once('wp-bootstrap-navmenu.php');
//180行之后的内容不要编辑
function cat_list2($category ,$count,$cat_img,$len_content,$len_title, $num){
?>
<?php query_posts("showposts=$count&cat=$category")?>
<div  class="widget widget_nav_menu panel panel-niit panel-primary">
  <div class="arrow"><a href="<?php echo get_category_link($category);?>">
  <span class="dashicons dashicons-portfolio"></span></a>
</div>
<div class="panel-heading widgettitle">
  <a class="panel-header" href="<?php echo get_category_link($category);?>">
    <?php single_cat_title();  ?>
  </a>
</div>
<ul > 
  <!--            <ul class="menu">-->
  <?php
  $i = 0;
  while (have_posts()) : the_post();
  if( $len_content > 0 and $i == 0) {
  ?>
  
  <img src="<?php if ($cat_img == "") {bloginfo('template_url'); ?>/images/cat5.jpg"<?php }else{echo $cat_img.'"';} ?> alt="" width="108px" height="80px">
  <div class="picat">
    <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">
      <?php
      echo mb_strimwidth(get_the_title(),0,$len_title-4,"...");
    ?></a><br />
    <?php ///显示字数限制的文章标题结束 ?>
    <?php
    echo mb_strimwidth(strip_tags(apply_filters('the_content', get_the_content())), 0, $len_content,"...");
    ?>
    <a target="_blank" href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>" class="orange">[详细]</a>
  </div>
  <hr />
  <?php
  }
  else {
  ?>
  <!--              <li class="menu-item menu-item-type-custom menu-item-object-custom">-->
  <div >
    <?php
    $post_id = get_the_ID();
    
    $link_url=get_post_meta($post_id, 'link_url', $single = true);
    if (  !$link_url ) {
    //echo '<a target=_blank href="'.$link_url .'" title="';
      //the_title_attribute();
      //echo '">';
      $link_url = get_the_permalink();
      //echo $link_url;
      }
      
      ?>
      <?php $data = catch_first_image(get_the_content()); if(empty($data)){ ?>
      <div class="s_box">
        <div class="s_img"><img src="<?php echo catch_that_image($num, $i) ?>"/></div>
        <div class="s_text">
          <p><a href="<?php echo $link_url;?>" title="<?php the_title_attribute();?>"> <?php
            echo mb_strimwidth(get_the_title(),0,$len_title,"...");
            //the_time('Y-m-d') ?>
          </a></p>
        </div>
      </div>
      <?php }else{?>
      <div class="s_box2">
        <div class="s_img2"><img src="<?php echo catch_that_image($num, $i) ?>"/></div>
        <div class="s_text2">
          <div class="s_text2_header">
            <p><a href="<?php echo $link_url;?>" title="<?php the_title_attribute();?>"> <?php
              echo mb_strimwidth(get_the_title(),0,$len_title,"...");
            //the_time('Y-m-d') ?> </a></p>
          </div>
          <div class="s_text_content">
            <p> <?php echo catch_the_data();
            ?>
            </p>
          </div>
        </div>
      </div>
      <?php } ?>
      <div class="clear_float"></div>
    </div>
    <?php
    }
    $i = $i+1;
    endwhile; ?>
  </ul>
  
</div>
<?php
}

function catch_artical_desc(){

$content = get_the_content(__("Read more &raquo;", "wpbootstrap"));
$c_len = mb_strlen($content);
$data1 = '';
if ($c_len > 100){
$data1 = mb_substr(strip_tags($content), 0, 100);
}
else{
$data1 = strip_tags($content);
}
if(empty($data1)){
return '...';
}else{
return $data1.'...';
}
}
////以下内容不要编辑

function cat_list($category ,$count,$cat_img,$len_content,$len_title){
  $img_width = "80px";
  $img_height = "80px";
  return cat_list_img($category ,$count,$cat_img,$len_content,$len_title,$img_width,$img_height);
}

function cat_list_img($category ,$count,$cat_img,$len_content,$len_title,$img_width,$img_height){
  ob_start();
?>
<?php query_posts("showposts=$count&cat=$category")?>
<div  class="widget widget_nav_menu panel panel-niit panel-primary">
  
<div class="panel-heading widgettitle">
  <a class="panel-header" href="<?php echo get_category_link($category);?>">
    <?php single_cat_title();  ?>
    <div class="right_title">
      <span  class="dashicons dashicons-portfolio"></span>
    </div>
  </a>
</div>
<ul class="menu">
  <?php
  $i = 0;
  while (have_posts()) : the_post();
  if( $len_content > 0 and $i == 0) {
    if (empty($cat_img)) $cat_img = catch_first_image(get_the_content( ));
    if (empty($cat_img)) $cat_img = get_stylesheet_directory_uri()."/images/cat5.jpg";
  ?>
  
  <div class="media">
  <a class="pull-left" href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">
    <img style="width: <?php echo $img_width; ?>; height: <?php echo $img_height; ?>" 
    class="media-object" src="<?php echo $cat_img; ?>" alt="" width="108px" height="80px">
  </a>
  <div class="media-body">
    <h4 class="media-heading">
    <a href="<?php the_permalink() ?>">
      <?php
      echo mb_strimwidth(get_the_title(),0,$len_title-4,"...");
    ?></a>
    <?php ///显示字数限制的文章标题结束 ?>
    </h4>
    <?php
    echo mb_strimwidth(strip_tags(apply_filters('the_content', get_the_content())), 0, $len_content,"...");
    ?>
    <a target="_blank" href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>" class="orange">[详细]</a>
  </div>
  </div>

  <?php
  }
  else {
  ?>
  <li class="menu-item menu-item-type-custom menu-item-object-custom ">
    <?php
    $post_id = get_the_ID();
    
    $link_url=get_post_meta($post_id, 'link_url', $single = true);
    if (  !$link_url ) {
      $link_url = get_the_permalink();
    }  
    ?>
      <a href="<?php echo $link_url;?>" title="<?php the_title_attribute();?>">
        <?php
        echo mb_strimwidth(get_the_title(),0,$len_title,"...");
        //the_time('Y-m-d') ?>
      </a></li>
      <?php
      }
      $i = $i+1;
      endwhile; ?>
    </ul>
    
  </div>
  <?php
  $content = ob_get_contents();
  ob_end_clean();
  return $content;
  }
  //得到缩略图
  function catch_first_image($content) {
  $first_img = '';
  $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $content, $matches);
  $first_img = @$matches [1] [0];
  if(empty($first_img)){ //Defines a default image
  return '';
  }
  return $first_img;
}

class vpn358_nav_walker extends Walker_Nav_Menu
{
function start_lvl( &$output, $depth = 0, $args = array() ) {
                  $indent = str_repeat("\t", $depth);
                  $output .= "\n$indent<ul class=\"dropdown-menu\">\n";
     }
}

function register_vpn358_menus() {
  register_nav_menus(
    array('primary_menu' => __( '主菜单' ) )
  );
}
add_action( 'init', 'register_vpn358_menus' );


function book_tab2acc($category,$count,$id,$show_desc=false){
  ob_start();
$len_title = 6;
$len_content = 135;
query_posts("showposts=$count&cat=$category");
$i = 0;
$book_imgs  = array();
while (have_posts()) : the_post();
$content = get_the_content();
$txt =strip_tags($content);
if (mb_strlen($txt)>$len_content) {
$txt = mb_substr( $txt, 0, $len_content);
$txt = $txt."...";
}
$image = addslashes(catch_first_image($content) );
$book_imgs[$i] = array("title"=>get_the_title(),
"img"=>$image,
"link"=>get_the_permalink(),
"txt"=>$txt);
$i++;
endwhile;
//var_export($book_imgs);
?>
<div id="<?php echo 'tab2acc-'.$id;?>">
  <ul class="resp-tabs-list">
    <?php
    $ac = 0;
    foreach ($book_imgs as $key => $book) {
    if($ac == 0 ) {
    $active = "active";
    $ac = 1;
    }else{
    $active="";
    }
    $title = $book['title'];
    if (mb_strlen($title)>$len_title) {
    $title = mb_substr($title, 0, $len_title)."...";
    }
    echo '<li>'.$title.'</li>';
    }
    ?>
  </ul>
  <div class="resp-tabs-container">
    <?php
    $ac = 0;
    foreach ($book_imgs as $key => $book) {
    if($ac == 0 ) {
    $active = "active";
    $ac = 1;
    }else{
    $active="";
    }
    $title = $book['title'];
    if (mb_strlen($title)>$len_title) {
    //$title = mb_substr($title, 0,$len_title);
    }
    $image = '<div class="row-fluid"><div class="span6"><a target="_blank" href="'.$book["link"].'"><img height="250px" src="'.stripcslashes($book["img"]).'"></a></div>';
    
    $content = $book["txt"];
    echo '<div>'.$image.'<div class="span6"><a target="_blank" href="'.$book["link"].'"><h3>'.$title.'</h3></a><p>'.$content.'</p></div></div></div>';
    }
    ?>
  </div>
</div>
<script type="text/javascript">
jQuery('#<?php echo 'tab2acc-'.$id;?>').easyResponsiveTabs({type: 'vertical',fit: true});
</script>
<?php
  $flush_content = ob_get_contents();
  ob_end_clean();
  return $flush_content;
}
function book_carousel($category,$count,$id,$show_desc=false){
query_posts("showposts=$count&cat=$category");
$i = 0;
$book_imgs  = array();
while (have_posts()) : the_post();
$content = get_the_content();
$txt = mb_substr( strip_tags($content), 0, 300);
$image = addslashes(catch_first_image($content) );
$book_imgs[$i] = array("title"=>get_the_title(),
"img"=>$image,
"link"=>get_the_permalink(),
"txt"=>$txt);
$i++;
endwhile;
//var_export($book_imgs);
?>
<div id="carousel-book-<?php echo $id;?>" style="height:370px;" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <?php
    $ac = 0;
    foreach ($book_imgs as $key => $book) {
    if($ac == 0 ) {
    $active = "active";
    $ac = 1;
    }else{
    $active="";
    }
    echo '<li data-target="#carousel-book-'.$id.'" data-slide-to="'.$key.'" class="'.$active.'"></li>';
    }
    ?>
  </ol>
  <div class="carousel-inner">
    <?php
    $ac = 0;
    foreach ($book_imgs as $key => $book) {
    if($ac == 0 ) {
    $active = "active";
    $ac++;
    }else{
    $active="";
    }
    ?>
    <div class="item <?php echo $active;?>">
      
      <div class="row-fluid">
        <div class="span4 clearfix">
          <a href="<?php echo $book['link'];?>">
            <img class=""
            style="margin-top:10px;"
            data-src="holder.js/350x350/auto/#777:#555/text:<?php echo $book['title'];?> "
            alt="<?php echo $book['title'];?>"
            src="<?php echo $book['img'];?>" width="350px" height="350px">
          </a>
        </div>
        <?php if ($show_desc == true) { ?>
        <div class="span8 clearfix">
          <div class="">
            <a href="<?php echo $book['link'];?>">
              <h3><?php echo $book['title'];?></h3>
            </a>
            <p><?php echo $book['txt'];?>...<br />
            <a href="<?php echo $book['link'];?>">(阅读更多)</a></p>
            
          </div>
        </div>
        <?php }; ?>
        
      </div>
    </div>
    <?php
    }
    ?>
  </div>
  <a class="left carousel-control" href="#carousel-book-<?php echo $id;?>" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
  </a>
  <a class="right carousel-control" href="#carousel-book-<?php echo $id;?>" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
  </a>
</div>
<?php
}

function register_button( $buttons ) {
   array_push( $buttons, "|", "recentposts" );
   return $buttons;
}

function add_plugin( $plugin_array ) {
   $plugin_array['recentposts'] = get_stylesheet_directory_uri() . '/vpn358_func/vpn358_short.js';
   return $plugin_array;
}
function my_recent_posts_button() {
 
   if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') ) {
      return;
   }
 
   if ( get_user_option('rich_editing') == 'true' ) {
      add_filter( 'mce_external_plugins', 'add_plugin' );
      add_filter( 'mce_buttons', 'register_button' );
   }
 
}
add_action('init', 'my_recent_posts_button');
///

function vpn358_register_sidebars() {
register_sidebar(array(
'id' => 'sidebar1',
'name' => 'Main Sidebar',
'description' => 'Used on every page BUT the homepage page template.',
'before_widget' => '<div id="%1$s" class="widget %2$s panel-niit panel-primary">',
'after_widget' => '</div>',
'before_title' => '<div class="panel-heading widgettitle">',
'after_title' => '</div>',
));
register_sidebar(array(
'id' => 'sb_homepage',
'name' => 'Homepage Sidebar',
'description' => 'Used only on the homepage page template.',
'before_widget' => '<div id="%1$s" class="widget %2$s panel-niit panel-primary">',
'after_widget' => '</div>',
'before_title' => '<h4 class="widgettitle">',
'after_title' => '</h4>',
));
register_sidebar(array(
'id' => 'footer1',
'name' => 'Footer 1',
'before_widget' => '<div id="%1$s" class="widget span4 %2$s">',
'after_widget' => '</div>',
'before_title' => '<h4 class="widgettitle">',
'after_title' => '</h4>',
));
}
vpn358_register_sidebars();
//前台启用图标字体
add_action( 'wp_enqueue_scripts', 'vpn358_load_dashicons' );
function vpn358_load_dashicons() {
  wp_enqueue_style( 'dashicons' );
}
// enqueue styles
if( !function_exists("vpn358_theme_styles") ) {
  function vpn358_theme_styles() {
  // This is the compiled css file from LESS - this means you compile the LESS file locally and put it in the appropriate directory if you want to make any changes to the master bootstrap.css.
  wp_register_style( 'Tabs-to-Accordion', get_stylesheet_directory_uri() . '/css/easy-responsive-tabs.css', array(), '1.0', 'all' );
  wp_enqueue_style( 'Tabs-to-Accordion' );
  }
}
add_action( 'wp_enqueue_scripts', 'vpn358_theme_styles' );
// enqueue javascript
if( !function_exists( "vpn358_theme_js" ) ) {
  function vpn358_theme_js(){
  wp_register_script( 'Tabs-to-Accordion',get_stylesheet_directory_uri() . '/js/easyResponsiveTabs.js',array('jquery'),'1.2' );
  wp_enqueue_script('Tabs-to-Accordion');
  }
}
add_action( 'wp_enqueue_scripts', 'vpn358_theme_js' );
//禁用Open Sans
class Disable_Google_Fonts {
public function __construct() {
add_filter( 'gettext_with_context', array( $this, 'disable_open_sans'             ), 888, 4 );
}
public function disable_open_sans( $translations, $text, $context, $domain ) {
if ( 'Open Sans font: on or off' == $context && 'on' == $text ) {
$translations = 'off';
}
return $translations;
}
}
$disable_google_fonts = new Disable_Google_Fonts;

?>