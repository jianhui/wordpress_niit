<?php
/**
 * The template for displaying Category pages
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

    <section id="primary" class="content-area">
        <div id="content" class="site-content" role="main">

            <?php if ( have_posts() ) : ?>

                <header class="archive-header">

                    <?php
                    $link_url = get_the_permalink();
                    // Show an optional term description.
                    $term_description = term_description();
                    if ( ! empty( $term_description ) ) :
                        printf( '<div class="taxonomy-description">%s</div>', $term_description );
                    endif;
                    ?>
                </header><!-- .archive-header -->

                <?php
                // Start the Loop.
                while ( have_posts() ) : the_post(); ?>
                    <div  class="container">
                        <div class="row-fluid">
                            <div class="span10 offset3">
                                <div class="list">
                                    <div class="list_img span4" id="list_img">
                                        <img class="img-polaroid" src="<?php echo(catch_first_image(get_the_content())); ?>" alt=""/>
                                    </div>
                                    <div class="span6">
                                            <div class="list_title">
                                                <h2> <a href="<?php echo $link_url;?>"><?php the_title() ?></a></h2>
                                            </div>
                                            <div class="list_content">
                                                <p class="lead"><?php echo catch_the_data(); ?></p>
                                            </div>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
<!--                    <h2>--><?php //the_title() ?><!--</h2>-->
<!---->
<!--                    <h2>--><?php //echo(catch_first_image(get_the_content())); ?><!--11</h2>-->
<!--                    <h3>--><?php //the_content() ?><!--</h3>-->


                 <?php get_template_part( 'content', get_post_format() );

                endwhile;
                // Previous/next page navigation.

            else :
                // If no content, include the "No posts found" template.
                get_template_part( 'content', 'none' );

            endif;
            ?>
        </div><!-- #content -->
    </section><!-- #primary -->


<?php get_footer(); ?>