<?php get_header();

if(file_exists(dirname(__FILE__).'/config_index.php')){
  require_once('config_index.php');
}

?>
<div id="content" class="container">
    <div class="row-fluid">
        <div id="main" class="span8 clearfix" role="main">
            <div class="row-fluid">
                <?php search_tab("opac.niit.edu.cn", "www.duxiu.com.j.niit.edu.cn", "lib.niit.edu.cn/xd/Public/yidu_edu.html"); ?>

            </div>
            
            <div class="row-fluid">
            <?php
                echo book_tab2acc($book_carousel_cat, $book_carousel_count, $book_carousel_id, $book_carousel_show_desc);
            ?>
            </div>

            <div class="row-fluid">
                <?php
                foreach ($cats as $cat) {
                    echo '<div class="span4">';
                    $category = $cat['cat_id'];
                    $cat_img = $cat['cat_image'];
                    $len_content = $cat['length_content'];
                    $count = $cat['count'];
                    echo cat_list($cat['cat_id'], $cat['count'], $cat['cat_image'], $cat['length_content'], $len_title, $cat['num']);
                    echo '</div>';
                }
                ?>
            </div>
            
        </div>
        <!-- end #main -->

        <?php get_sidebar("sb_homepage"); // sidebar 1 ?>

    </div>
    <!-- end row-fluid -->
</div> <!-- end #content -->


<?php get_footer(); ?>
