<?php
function vpn358_search_box($atts) {
	extract(shortcode_atts(array(
		'opac' => "opac.niit.edu.cn",
		'duxiu' => "www.duxiu.com",
		'yidu'=> "lib.niit.edu.cn/xd/Public/yidu_edu.html",
	), $atts));
	return search_tab($opac, $duxiu, $yidu); 
}
add_shortcode('vpn358_search_box', 'vpn358_search_box');

function vpn358_simple_list_cats($atts) {
	extract(shortcode_atts(array(
		'cats_id' => '9,10',
		'count' => 6,
		'cat_img'=>'',
		'len_content' => 0,
		'len_title' => 20, 
		'span' => 'span4',
		'img_width' => '64px',
		'img_height' => '64px',
	), $atts));
	$cats = explode(',', $cats_id);
	$content="";
		
	foreach ($cats as $cat) {
		$info = "";
        $content .= '<div class="'.$span.'">';
        $category = $cat;
        $content .=  cat_list($category ,$count,$cat_img,$len_content,$len_title,$img_width,$img_height);
        $content .= '</div>';
        
    }  
	 return '<div class="row-fluid">'.$content.'</div>';
}
add_shortcode('simple_list_cats', 'vpn358_simple_list_cats');

function vpn358_book_tab2acc($atts) {
	extract(shortcode_atts(array(
		'cat_id' => '13',
		'count' => 5,
		'id' => "book_tab2acc_".rand(),
		//'len_title' => 20, 
		'show_desc' => false,
	), $atts));

    $content =   book_tab2acc($cat_id,$count,$id,$show_desc);
	return $content;
}
add_shortcode('book_tab2acc', 'vpn358_book_tab2acc');
?>