#wordpress_niit
图书馆的Wordpress主题，采用bootstrap3，基于https://github.com/320press/wordpress-bootstrap 开发
期望的功能有
+ 适应手机端
+ 自定义布局
+ 新书推荐、文档列表、资源导航等功能
+ 一些shortcode的说明
http://git.oschina.net/jianhui/wordpress_niit/issues/1

####项目宗旨、起因
各大高校、高职图书馆，每年要建好几个网站，图书馆的一些需求与普通网站并不相同，
如专家文库、机构库等功能，与普通cms相比，只是多了一些字段，但很多cms通常并不提供，
而这些专家文库、机构库等功能却花费不菲。

在图书馆工作这么多年，发现两点：

1. 图书馆的很多功能是重复的，因此希望做一个完善一些的网站，
能够满足图书馆绝大部分常规需求，并能提供足够强大的二次开发功能。

2. 图书馆的cms要提供一些专题设计功能，例如开了一次读书节，开展了一次真人图书馆活动，
希望做一个像网站首页一样的专题，有图片、有导读、有内容列表，而当前的cms往往缺少专题设计功能，
内页只能提供一些列表式的内容显示，比较遗憾。

解决方案：
针对以上两点，Wordpress对图书馆而言实在是非常比较理想的建站工具，海量的插件，完善的二次开发接口，功能非常强大。
唯一缺少的，就是一些图书馆的常用功能，因此，我希望把图书馆常用图书检索、opac登录、资源导航等功能做成通用组件，
免去大家二次开发的麻烦。

其次，根据图书馆的实际情况，把一些组件、常用功能作用shortcode，利用Wordpress的shortcode功能，
可以在page中二次设计一些专题。

####为何基于customizr
1. 因为要适配手机端，比较理想的方案是采用bootstrap开发；
2. 而基于bootstrap的Wordpress主题中，很多代码比较繁杂，很多功能用不到，但要理解其代码要花费太多时间
3. customizr不是一开始选择的最佳方案，之前选择的是320press，但320press基于bootstrap32，不兼容ie8，不得不淘汰
4. 仓促之间，以前用过customizr，于是就基于customizr开发了，基本上重写了index、footer、catagory、page和single，下面想重写menu，但其menu支持手机自适应，因此一时半会儿不会换掉

####一期项目目标：
+ 与当前图书馆主页http://lib.niit.edu.cn 布局类似设计，完成到新主页的迁移

####二期目标：
+ 仿百度文库的在线阅读功能，可以用于图书馆专家文库建设
上传一篇pdf文档，自动转换格式，可以像百度文库一样供访问者阅读，采用flexreader组件
+ 这部分代码已经完成，下面就要集成到主题里面

* 将图书馆常用的功能封装成组件，并以shortcode的方式，方便管理员Wordpress二次设计一些页面，当前支持的shortcode有
[book_tab2acc cat_id=13 id=sample_id]，用于展示图书，电脑上浏览是tab方式的，手机浏览是手风琴方式的
* [vpn358_search_box opac=opac.niit.edu.cn duxiu=www.duxiu.com yidu=lib.niit.edu.cn/xd/Public/yidu_edu.html]
这个是检索框，opac、duxiu、yidu都知道的，opac和读秀是网址，duxiu可以为www.duxiu.com.test.vpn358.com，可以校内统计；yidu是calis的yidu整合文件，以iframe方式集成
* [simple_list_cats cats_id=30,13 span=span6 len_content=100 img_width=64px img_height=64px]
span=span6是行数，如果cats_id是两个，则使用span6；如果cats_id是3个，则使用span4。
注意，只有len_content大于0的时候，img_width、img_height才有效，因为这时候才会显示图片。
len_content=0时，只显示标题，没有图片和内容简介。


####三期目标
* 不同分类，可以调用不同菜单，设置不同的sidebar
+ mega菜单，估计放到最后了

首页、page等页面需要的功能组件有

1. 好书推荐，一个响应式布局的tab，手机端浏览是转换为，tab栏在右侧，左侧显示图书封面，中间显示图书简介【已经完成】
2. 图书检索，包括opac检索、读秀检索、e读检索，管理员可以需要在配置文件中显示需要的，并可以iframe方式显示图书馆自己的检索【已经完成】

1. 分类列表，包括多种样式，标题纯列表式，分类图片左侧+标题列表，分类图片左侧+分类简介+标题列表，分类图片顶部+标题列表；分类图片来源于分类简介或者config文件的默认图片
2. 分类列表的图片显示，多种样式，grid显示分类每一篇文章的第一张图片，li显示图片+标题，li显示图片+标题+内容简介
3. 引用优酷等视频内容，根据读者浏览器，例如用手机浏览时，自动切换到相应视频的mp4
4. 图片+标题的slider

sidebar需要的组件

1. 时间轴的生平简介，用于介绍科学家等
2. 分类新闻，可以选择条数、类别
3. 带tab的新闻栏，可以显示多个分类的新闻栏
4. 读者opac系统登录

Wordpress分类页的显示样式

1. 默认的li标题列表，此不用开发
2. 标题+内容简介列表
3. 左侧图片+标题+内容简介
4. 视频缩略图+标题，grid样式

###使用方法
在wp-content/themes目录下运行命令
```
git clone https://git.oschina.net/jianhui/wordpress_niit.git
ln -s  customizr/vpn358 vpn358
```

####参与开发的要求
欢迎图书馆人员贡献代码，请在http://git.oschina.net 上注册账号，我把大家加入到项目中
+ 参与项目最好理解git的使用
git的使用：http://git.oschina.net/progit/
+ 每个人开发，请新建一个branch

####前期主要以功能开发为主，网页设计采用bootstrap3
+ 要求熟悉html+css网页设计.
html教程：http://www.w3cschool.cn/index-5.html
css教程：http://www.w3cschool.cn/index-7.html
+ 在前面的基础上，要求理解bootstrap3网页设计css框架，尤其是页面自定义布局这部分
bootstrap介绍和使用：http://v3.bootcss.com/getting-started/
+ 懂php代码的开发，要求不高，只要能看懂基本的代码即可
php教程：http://www.w3cschool.cn/index-36.html
+ 理解Wordpress的二次开发，要求懂得Wordpress的内置函数、挂钩（hook））、action的使用
Wordpress二次开发：http://blog.wpjam.com/article/wp-theme-lessons/

##测试环境搭建
+ 到 https://bitnami.com/stack/xampp#wordpress 下载windows下的Wordpress运行环境
+ 从http://git.oschina.net/jianhui/wordpress_niit 下载代码到本地，进行测试。Wordpress的使用请Google。
=======